#ifndef _STRING_UTILS_H_INCLUDED
#define _STRING_UTILS_H_INCLUDED

#define BUFFER_START_SIZE   24
const char* const string_format(const char* const text, ...);
#endif
