#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "kernel/boot.h"
#include "FreeRTOS.h"
#include "task.h"

#include "apps/eurobot/robot/atax.h"
#include "apps/eurobot/robot/macros.h"
#include "apps/eurobot/robot/odometry.h"
#include "apps/eurobot/robot/canbus_gateway.h"

#include "drivers/logger/logger.h"
#include "drivers/gpio/gpiolib.h"

#define LOG_TAG     "TEST"

bool valpin = true;
void j0_listener(uint8_t pin, uint8_t event)
{
    gpio_write_pin(96, valpin);
    valpin = !valpin;
}

static void __init testTask(void* params)
{
    ec_bus_init();
    odometry_init();
    vTaskDelay(2000);
    gpio_register_pin(97, GPIO_DIR_OUTPUT, false);
    gpio_register_pin(96, GPIO_DIR_OUTPUT, true);

    //gpio_register_listener(64, j0_listener);
    ////104

    odometry_move_straight(290, ODOMETRY_DIRECTION_FORWARD, 120);
    odometry_wait_additional(100);

    odometry_rotate(-27, 160);
    odometry_wait_additional(100);

    vTaskDelay(100);
    odometry_move_straight(120, ODOMETRY_DIRECTION_FORWARD, 50);
    odometry_wait_additional(100);

    odometry_move_straight(150, ODOMETRY_DIRECTION_BACKWARDS, 30);
    odometry_wait_additional(100);
    atax_servo_set_angle(DOOR_RIGHT_SERVO_ID, DOOR_RIGHT_POSITION_OPEN, 160, 1);
    atax_servo_set_angle(DOOR_LEFT_SERVO_ID, DOOR_LEFT_POSITION_OPEN, 160, 1);
    vTaskDelay(400);
    odometry_move_straight(150, ODOMETRY_DIRECTION_FORWARD, 80);
    odometry_wait_additional(100);

    odometry_rotate(-25, 50);
    odometry_wait_additional(100);
    odometry_move_straight(30, ODOMETRY_DIRECTION_BACKWARDS, 30);
    odometry_wait_additional(100);
    odometry_rotate(17, 50);
    odometry_move_straight(200, ODOMETRY_DIRECTION_FORWARD, 30);
    odometry_wait_additional(100);
    odometry_rotate(-20, 50);
    odometry_wait_additional(100);

    while(true);
    uint8_t val = 0;
    struct position position =
    {
        .x      = 1000,
        .y      = 0,
        .angle  = 0
    };

    vTaskDelay(500);
    while(1)
    {
        odometry_move_straight(1000, ODOMETRY_DIRECTION_FORWARD, 70);
        while(1);
    }
    while(1)
    {
        while(true) {
        position.x = 1900;
        position.y = 0;
        odometry_move_to_position(&position, 1, 60);
        while(odometry_get_status() != ODOMETRY_STATUS_IDLE)
            vTaskDelay(250);

        vTaskDelay(1000);
        position.x = 0;
        position.y = 0;
        odometry_move_to_position(&position, 1, 60);
        while(odometry_get_status() != ODOMETRY_STATUS_IDLE)
            vTaskDelay(250);
        vTaskDelay(1000);
        }

        vTaskDelay(1000);
        position.x = 1900;
        position.y = 1200;
        odometry_move_to_position(&position, 1, 60);
        while(odometry_get_status() != ODOMETRY_STATUS_IDLE)
            vTaskDelay(250);

        vTaskDelay(1000);
        position.x = 0;
        position.y = 1200;
        odometry_move_to_position(&position, 1, 60);
        while(odometry_get_status() != ODOMETRY_STATUS_IDLE)
            vTaskDelay(250);

        vTaskDelay(1000);
        position.x = 0;
        position.y = 0;
        odometry_move_to_position(&position, 1, 60);
        while(odometry_get_status() != ODOMETRY_STATUS_IDLE)
            vTaskDelay(250);
        odometry_set_angle(0, 60);
        while(odometry_get_status() != ODOMETRY_STATUS_IDLE)
            vTaskDelay(250);
        while(true);

/*        atax_gpio_set_value(1, 0, val);
        gpio_write_pin(97, val);
        val = !val;*/
    }

}

ADD_TO_THREAD_POOL(testTask) = testTask;
