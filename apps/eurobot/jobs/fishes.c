#include <stdint.h>
#include <stdbool.h>

#include "drivers/logger/logger.h"

#include "apps/eurobot/job.h"
#include "apps/eurobot/robot/odometry.h"
#include "apps/eurobot/robot/atax.h"

#include "apps/eurobot/robot/macros.h"

struct position fishes_start_position =
{
    .x  = 505,
    .y  = 1770
};

struct position fishes_pick_up_position =
{
    .x  = 1140,
    .y  = 1770
};

struct job_odometry_data fishes_start_odometry_data =
{
    .position  = &fishes_start_position,
    .speed     = 60,
    .direction =  ODOMETRY_DIRECTION_BACKWARDS
};

struct job_odometry_data fishes_pick_up_odometry_data =
{
    .position  = &fishes_pick_up_position,
    .speed     = 35,
    .direction = ODOMETRY_DIRECTION_BACKWARDS
};

struct job_dependency job_fish_pick_up_dependency =
{
    .id         = 5,
    .condition  = JOB_DEPENDENCY_UNLOCK_ON_SUCCESS
};

uint8_t actions_fish_start_position(uint8_t job_id)
{
    vTaskDelay(400);
    atax_servo_set_angle(DOOR_LEFT_SERVO_ID, DOOR_LEFT_POSITION_CLOSED, 130, 1);
    atax_servo_set_angle(DOOR_RIGHT_SERVO_ID, DOOR_RIGHT_POSITION_CLOSED, 130, 1);

    return 0;
}

uint8_t actions_fish_pick_up(uint8_t job_id)
{
    vTaskDelay(1400);
    atax_servo_set_angle(FISH_CATCHER_SERVO_ID, FISH_CATCHER_POSITION_DOWN, 160, 1);
    vTaskDelay(3000);
    atax_servo_set_angle(FISH_CATCHER_SERVO_ID, FISH_CATCHER_POSITION_UP, 260, 1);
    return 0;
}

struct job job_fish_start_position =
{
    .position           = &fishes_start_odometry_data,
    .actions            = actions_fish_start_position,
    .id                 = 5,
    .enabled            = true,
    .priority           = 9,
    .time_dependency    = 0,
    .total_dependencies = 0,
    .dependencies       = NULL
};

struct job job_pick_up_fishes =
{
    .position           = &fishes_pick_up_odometry_data,
    .actions            = actions_fish_pick_up,
    .id                 = 6,
    .enabled            = true,
    .priority           = 20,
    .time_dependency    = 0,
    .total_dependencies = 1,
    .dependencies       = &job_fish_pick_up_dependency
};


void add_fishes_jobs(void)
{
//    job_add_to_jobs_pool(&job_fish_start_position);
    //job_add_to_jobs_pool(&job_pick_up_fishes);
}
