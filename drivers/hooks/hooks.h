#ifndef _HOOKS_H_INCLUDED
#define _HOOKS_H_INCLUDED

#include <stdint.h>
#include "FreeRTOS.h"
#include "task.h"

#define MAX_STACK_OVERFLOW_HOOKS    3
#define MAX_MALLOC_FAILED_HOOKS     3
#define MAX_TICK_HOOKS              3

int8_t register_stack_overflow_hook(void (*hookf)(TaskHandle_t task, signed char* task_name));
int8_t register_malloc_failed_hook(void (*hookf)(void));
int8_t register_tick_hook(void (*hookf)(void));

void hooks_init(void);

#endif
