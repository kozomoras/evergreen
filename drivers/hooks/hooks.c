#include "drivers/hooks/hooks.h"

static void (*stack_overflow_hooks[MAX_STACK_OVERFLOW_HOOKS])(TaskHandle_t task, signed char* name);
static void (*malloc_failed_hooks[MAX_MALLOC_FAILED_HOOKS])(void);
static void (*tick_hooks[MAX_TICK_HOOKS])(void);

static uint8_t total_stack_overflow_hooks;
static uint8_t total_malloc_failed_hooks;
static uint8_t total_tick_hooks;

void hooks_init(void)
{
    total_tick_hooks = 0;
    total_stack_overflow_hooks = 0;
    total_malloc_failed_hooks = 0;
}

int8_t register_stack_overflow_hook(void (*hookf)(TaskHandle_t task, signed char* task_name))
{
    int8_t result = 0;
    if(total_stack_overflow_hooks == MAX_STACK_OVERFLOW_HOOKS)
    {
        result = -1;
        goto exit;
    }

    stack_overflow_hooks[total_stack_overflow_hooks++] = hookf;

exit:
    return result;
}

int8_t register_malloc_failed_hook(void (*hookf)(void))
{
    int8_t result = 0;
    if(total_malloc_failed_hooks == MAX_MALLOC_FAILED_HOOKS)
    {
        result = -1;
        goto exit;
    }

    malloc_failed_hooks[total_malloc_failed_hooks++] = hookf;

exit:
    return result;
}

int8_t register_tick_hook(void (*hookf)(void))
{
    int8_t result = 0;
    if(total_tick_hooks == MAX_TICK_HOOKS)
    {
        result = -1;
        goto exit;
    }

    tick_hooks[total_tick_hooks++] = hookf;

exit:
    return result;
}

void vApplicationMallocFailedHook(void)
{
    uint8_t i;
    for(i = 0; i < total_malloc_failed_hooks; i++)
    {
        malloc_failed_hooks[i]();
    }
}

void vApplicationTickHook(void)
{
    uint8_t i;
    for(i = 0; i < total_tick_hooks; i++)
    {
        tick_hooks[i]();
    }
}

void vApplicationStackOverflowHook(TaskHandle_t task, signed char* name)
{
    uint8_t i;
    for(i = 0; i < total_stack_overflow_hooks; i++)
    {
        stack_overflow_hooks[i](task, name);
    }
}

