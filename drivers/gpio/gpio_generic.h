#ifndef _GPIO_GENERIC_H_INCLUDED
#define _GPIO_GENERIC_H_INCLUDED

#include "evergreen/list_generic.h"

struct gpio_chip_info
{
    void    (*gpio_register_input)(uint8_t pin);
    void    (*gpio_register_output)(uint8_t pin);
    void    (*gpio_unregister)(uint8_t pin);
    void    (*gpio_write)(uint8_t pin, uint8_t value);
    void    (*gpio_request_isr)(uint8_t pin);
    void    (*gpio_release_isr)(uint8_t pin);
    int8_t  (*gpio_read)(uint8_t pin);

    uint8_t start_pin;
    uint8_t total_pins;

    char*   label;
};

void            gpio_module_init(void);
extern uint8_t  gpiochip_add(struct gpio_chip_info* gpio_chip_info);

#endif

