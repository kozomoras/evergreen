/*   * * * P R O P R I E T A R Y   A N D  C O N F I D E N T I A L * * *
 *   Author: Sava Jakovljev
 *   Date: March 2016
 *
 *   ALL RIGHTS RESERVED.
 *
 * */

#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <limits.h>

#include "drivers/gpio/gpiolib.h"
#include "drivers/gpio/gpio_generic.h"
#include "drivers/logger/logger.h"
#include "evergreen/list_generic.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "boot.h"

#define GPIO_NOTIFY_BIT_SUCCESS     (1 << 1)
#define GPIO_NOTIFY_BIT_FAILURE     (1 << 2)

#define GPIO_TAG                    "GPIO service"

enum gpio_request_code
{
    GPIO_REGISTER_INPUT,
    GPIO_REGISTER_OUTPUT,
    GPIO_REGISTER_LISTENER,
    GPIO_UNREGISTER_LISTENER,   /* TODO */
    GPIO_UNREGISTER,            /* TODO */
    GPIO_INPUT_ACTIVATED,
    GPIO_INPUT_LONG_PRESS_ACTIVATED,
    GPIO_INPUT_RELEASED,
    GPIO_WRITE_PIN,
    GPIO_READ_PIN
};

struct gpio_chip
{
    struct gpio_chip_info*  gpio_chip_info;
    struct list_head        list;
    uint8_t                 id;
};

struct gpio_service_request
{
    enum gpio_request_code request_code;
    uint8_t                pin;
    bool                   pulled_up;
    bool                   value;
    TaskHandle_t           calling_task_id;
    void                   (*listener)(uint8_t pin, uint8_t event);
};

struct gpio_input_listener
{
    uint8_t          pin;
    uint8_t          event;
    void             (*func)(uint8_t pin, uint8_t event);
    struct list_head list;
};

struct gpio_input
{
    uint8_t           pin;
    struct gpio_chip* parent;
    uint8_t           values[GPIO_DEBOUNCE_COUNT];
    bool              pulled_up;
    uint8_t           previous_state;
    TickType_t        press_start;
    struct list_head  list;
};

LIST_HEAD(gpio_chips);
LIST_HEAD(gpio_inputs);
LIST_HEAD(gpio_inputs_listeners);
static QueueHandle_t gpio_service_queue;


/*
 * Adds a new GPIO chip to the GPIO list queue.
 */
uint8_t gpiochip_add(struct gpio_chip_info* gpio_chip_info)
{
    struct gpio_chip* gpio_chip = (struct gpio_chip*)pvPortMalloc(sizeof(struct gpio_chip));
    if(gpio_chip == NULL)
        return 1;

    gpio_chip->gpio_chip_info = gpio_chip_info;
    list_add(&(gpio_chip->list), &gpio_chips);

    return 0;
}

uint8_t gpiochip_remove(struct gpio_chip_info* gpio_chip_info)
{
    struct gpio_chip* tmp;

    list_for_each_entry(tmp, &gpio_chips, list)
    {
        if(tmp->gpio_chip_info == gpio_chip_info)
        {
            list_del(&tmp->list);

            /* TODO: should delete all inputs associated with this chip.
             * gpio_input_remove should be called,
             * and gpio_unregister_listener should be called.
             * */
            break;
        }
    }

    vPortFree(tmp);

    return 0;
}

static bool gpiochip_pin_is_valid(uint8_t pin)
{
    struct gpio_chip* tmp;

    list_for_each_entry(tmp, &gpio_chips, list)
    {
        if(pin > tmp->gpio_chip_info->start_pin && tmp->gpio_chip_info->start_pin + tmp->gpio_chip_info->total_pins < pin)
            return true;
    }

    return false;
}


static struct gpio_input* gpio_find_input(uint8_t pin)
{
    struct gpio_input* tmp;
    list_for_each_entry(tmp, &gpio_inputs, list)
    {
        if(tmp->pin == pin)
            return tmp;
    }

    return NULL;
}

static struct gpio_chip* gpio_find_chip_by_pin(uint8_t pin)
{
    struct gpio_chip* tmp;
    list_for_each_entry(tmp, &gpio_chips, list)
    {
        if(pin > tmp->gpio_chip_info->start_pin && tmp->gpio_chip_info->start_pin + tmp->gpio_chip_info->total_pins > pin)
            return tmp;
    }

    return NULL;
}

uint8_t gpio_register_pin(uint8_t pin, uint8_t direction, bool pulled_up)
{
    struct gpio_service_request request =
    {
        .request_code    = (direction == GPIO_DIR_INPUT) ? GPIO_REGISTER_INPUT : GPIO_REGISTER_OUTPUT,
        .pin             = pin,
        .pulled_up       = pulled_up,
        .calling_task_id = xTaskGetCurrentTaskHandle(),
        .value           = false,
        .listener        = NULL
    };

    while(xQueueSend(gpio_service_queue, (const void*)&request, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    /* here it should wait for task notify by gpio service */
    uint32_t notified_value;
    xTaskNotifyWait(GPIO_NOTIFY_BIT_SUCCESS | GPIO_NOTIFY_BIT_FAILURE,
            ULONG_MAX,
            &notified_value,
            portMAX_DELAY);
    return -(notified_value & GPIO_NOTIFY_BIT_FAILURE);
}

uint8_t gpio_write_pin(uint8_t pin, bool value)
{
    struct gpio_service_request request =
    {
        .request_code = GPIO_WRITE_PIN,
        .calling_task_id = xTaskGetCurrentTaskHandle(),
        .pin = pin,
        .value = value
    };

    while(xQueueSend(gpio_service_queue, (const void*)&request, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);


    uint32_t notified_value;
    xTaskNotifyWait(GPIO_NOTIFY_BIT_SUCCESS | GPIO_NOTIFY_BIT_FAILURE,
            ULONG_MAX,
            &notified_value,
            portMAX_DELAY);
    return -(notified_value & GPIO_NOTIFY_BIT_FAILURE);
}

uint8_t gpio_register_listener(uint8_t pin, void (*listener)(uint8_t pin, uint8_t event))
{
    struct gpio_service_request request =
    {
        .request_code    = GPIO_REGISTER_LISTENER,
        .calling_task_id = xTaskGetCurrentTaskHandle(),
        .pin             = pin,
        .listener        = listener
    };

    while(xQueueSend(gpio_service_queue, (const void*)&request, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    uint32_t notified_value;
    xTaskNotifyWait(GPIO_NOTIFY_BIT_SUCCESS | GPIO_NOTIFY_BIT_FAILURE,
            ULONG_MAX,
            &notified_value,
            portMAX_DELAY);
    return -(notified_value & GPIO_NOTIFY_BIT_FAILURE);
}

uint8_t gpio_unregister_listener(uint8_t pin, void (*listener)(uint8_t pin, uint8_t event))
{
    struct gpio_service_request request =
    {
        .request_code    = GPIO_UNREGISTER_LISTENER,
        .calling_task_id = xTaskGetCurrentTaskHandle(),
        .pin             = pin,
        .listener        = listener
    };

    while(xQueueSend(gpio_service_queue, (const void*)&request, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    uint32_t notified_value;
    xTaskNotifyWait(GPIO_NOTIFY_BIT_SUCCESS | GPIO_NOTIFY_BIT_FAILURE,
            ULONG_MAX,
            &notified_value,
            portMAX_DELAY);
    return -(notified_value & GPIO_NOTIFY_BIT_FAILURE);
}

uint8_t gpio_read_pin(uint8_t pin)
{
    struct gpio_service_request request =
    {
        .request_code    = GPIO_READ_PIN,
        .calling_task_id = xTaskGetCurrentTaskHandle(),
        .pin             = pin
    };

    while(xQueueSend(gpio_service_queue, (const void*)&request, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    uint32_t notified_value;
    xTaskNotifyWait(ULONG_MAX,
            ULONG_MAX,
            &notified_value,
            portMAX_DELAY);
    return notified_value;
}

static uint8_t __gpio_write_pin(uint8_t pin, bool value)
{
    struct gpio_chip* chip = gpio_find_chip_by_pin(pin);
    if(chip == NULL)
        return GPIO_NOTIFY_BIT_FAILURE;

    chip->gpio_chip_info->gpio_write(pin, value);
    return GPIO_NOTIFY_BIT_SUCCESS;
}

static uint8_t __gpio_read_pin(uint8_t pin)
{
    struct gpio_input* input;
    list_for_each_entry(input, &gpio_inputs, list)
    {
        if(input->pin == pin)
            return input->previous_state;
    }

    return GPIO_NOTIFY_BIT_FAILURE;
}

static uint8_t __gpio_register_pin(uint8_t pin, uint8_t direction, bool pulled_up)
{
    struct gpio_chip* chip = gpio_find_chip_by_pin(pin);
    if(chip == NULL)
        return GPIO_NOTIFY_BIT_FAILURE;

    struct gpio_input* input = gpio_find_input(pin);
    if(input != NULL)
    {
        return direction == GPIO_DIR_INPUT ? GPIO_NOTIFY_BIT_SUCCESS :
                                             GPIO_NOTIFY_BIT_FAILURE;
    }

    if(direction == GPIO_DIR_OUTPUT)
    {
        chip->gpio_chip_info->gpio_register_output(pin);
        return GPIO_NOTIFY_BIT_SUCCESS;
    }

    chip->gpio_chip_info->gpio_register_input(pin);
    input = (struct gpio_input*)pvPortMalloc(sizeof(struct gpio_input));
    input->pin = pin;
    input->parent = chip;
    input->pulled_up = pulled_up;
    list_add(&(input->list), &gpio_inputs);
exit:
    return GPIO_NOTIFY_BIT_SUCCESS;
}

static uint8_t __gpio_register_listener(uint8_t pin, void (*listener)(uint8_t pin, uint8_t event))
{
    struct gpio_chip* chip = gpio_find_input(pin);
    if(chip == NULL || listener == NULL)
        return GPIO_NOTIFY_BIT_FAILURE;

    struct gpio_input_listener* new_listener = (struct gpio_input_listener*)
                                              pvPortMalloc(sizeof(struct gpio_input_listener));
    if(new_listener == NULL)
        return GPIO_NOTIFY_BIT_FAILURE;
    new_listener->pin = pin;
    new_listener->func = listener;
    list_add(&(new_listener->list), &gpio_inputs_listeners);

    return GPIO_NOTIFY_BIT_SUCCESS;
}

static uint8_t __gpio_unregister_listener(uint8_t pin, void (*listener_func)(uint8_t pin, uint8_t event))
{
    struct gpio_input_listener* listener;
    list_for_each_entry(listener, &gpio_inputs_listeners, list)
    {
        if(listener->pin == pin && listener->func == listener_func)
        {
            list_del(&(listener->list));
            vPortFree(listener);
            return GPIO_NOTIFY_BIT_SUCCESS;
        }
    }

    return GPIO_NOTIFY_BIT_FAILURE;
}


void listeners_thread(void* params);
static void gpio_activate_listeners(uint8_t pin, uint8_t event)
{
    struct gpio_input_listener* listener;

    list_for_each_entry(listener, &gpio_inputs_listeners, list)
    {
        if(listener->pin == pin)
        {
            //listener->func(pin, event);
            listener->event = event;
            xTaskCreate(listeners_thread, "", configMINIMAL_STACK_SIZE, (void*)listener, tskIDLE_PRIORITY, NULL);
        }
    }
}


void gpio_service(void* params)
{
    struct gpio_service_request request;
    uint8_t result;

    while(true)
    {
        while(xQueueReceive(gpio_service_queue, (void*)&request, portMAX_DELAY) != pdTRUE)
        {
            vTaskDelay(1);
        }

        switch(request.request_code)
        {
        case GPIO_REGISTER_INPUT:
            result = __gpio_register_pin(request.pin, GPIO_DIR_INPUT, request.pulled_up);
            xTaskNotify(request.calling_task_id, (uint32_t)result, eSetValueWithOverwrite);
            break;
        case GPIO_REGISTER_OUTPUT:
            result = __gpio_register_pin(request.pin, GPIO_DIR_OUTPUT, false);
            xTaskNotify(request.calling_task_id, (uint32_t)result, eSetValueWithOverwrite);
            break;
        case GPIO_WRITE_PIN:
            result = __gpio_write_pin(request.pin, request.value);
            xTaskNotify(request.calling_task_id, (uint32_t)result, eSetValueWithOverwrite);
            break;
        case GPIO_READ_PIN:
            result = __gpio_read_pin(request.pin);
            xTaskNotify(request.calling_task_id, (uint32_t)result, eSetValueWithOverwrite);
            break;
        case GPIO_REGISTER_LISTENER:
            result = __gpio_register_listener(request.pin, request.listener);
            xTaskNotify(request.calling_task_id, (uint32_t)result, eSetValueWithOverwrite);
            break;
        case GPIO_UNREGISTER_LISTENER:
            result = __gpio_unregister_listener(request.pin, request.listener);
            xTaskNotify(request.calling_task_id, (uint32_t)result, eSetValueWithOverwrite);
        case GPIO_INPUT_ACTIVATED:
        case GPIO_INPUT_LONG_PRESS_ACTIVATED:
        case GPIO_INPUT_RELEASED:
            debug(GPIO_TAG, "Activating GPIO input %d", request.pin);
            gpio_activate_listeners(request.pin, request.request_code);
        }
    }
}


static void gpio_send_request_from_isr(enum gpio_request_code request_code, uint8_t pin)
{
    struct gpio_service_request request =
    {
        .request_code = request_code,
        .pin = pin
    };

    xQueueSendToBackFromISR(gpio_service_queue, (const void*)&request, NULL);
}

static void gpio_read_debounce_array(struct gpio_input* input)
{
    volatile uint8_t state = true;
    volatile uint8_t i = 0;
    while(i < GPIO_DEBOUNCE_COUNT - 1)
    {
        if((state = (input->values[i] == input->values[i + 1])) == 0)
            break;
        i++;
    }
    state = input->pulled_up == 0 ? (input->values[0]) : !(input->values[0]);

    if(state == 0)
    {
        if(input->previous_state)
        {
            /* INPUT RELEASED */
            gpio_send_request_from_isr(GPIO_INPUT_RELEASED, input->pin);
        }
    }
    else
    {
        if(input->previous_state == 0)
        {
            /* INPUT ACTIVATED */
            gpio_send_request_from_isr(GPIO_INPUT_ACTIVATED, input->pin);
            input->press_start = xTaskGetTickCount();
        }
        else
        {
            if(xTaskGetTickCount() - input->press_start > 1000 * GPIO_LONG_PRESS_TIME_SEC)
            {
                gpio_send_request_from_isr(GPIO_INPUT_LONG_PRESS_ACTIVATED, input->pin);
                input->press_start = xTaskGetTickCount();
            }
        }
    }

    input->previous_state = state;
}

static void gpio_debounce(void)
{
    volatile uint8_t i;
    static volatile uint8_t debouncer = 0;
    volatile struct gpio_input* input;

    list_for_each_entry(input, &gpio_inputs, list)
    {
        input->values[debouncer] = input->parent->gpio_chip_info->gpio_read(input->pin);
        gpio_read_debounce_array(input);
    }

    if(++debouncer == GPIO_DEBOUNCE_COUNT)
        debouncer = 0;
}

void gpio_service_init(void)
{
    gpio_module_init();
    register_tick_hook(gpio_debounce);
    gpio_service_queue = xQueueCreate(GPIO_QUEUE_SIZE, sizeof(struct gpio_service_request));
    xTaskCreate(gpio_service, "", configMINIMAL_STACK_SIZE, NULL, configMAX_PRIORITIES - 1, NULL);
}

void listeners_thread(void* params)
{
    struct gpio_input_listener* listener = (struct gpio_input_listener*)params;
    listener->func(listener->pin, listener->event);
    vTaskDelete(NULL);
}
