#ifndef _TIMELIB_H_INCLUDED
#define _TIMELIB_H_INCLUDED

#include <stdint.h>

uint8_t timer_start_oneshot(uint8_t id, uint32_t period, void (*callback)(uint8_t id, uint8_t skips));
uint8_t timer_start_repeating(uint8_t id, uint32_t period, void (*callback)(uint8_t id, uint8_t skips));
uint8_t timer_cancel(uint8_t id);

#endif
