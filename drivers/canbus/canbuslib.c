#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <limits.h>

#include "drivers/canbus/canbus_generic.h"
#include "drivers/canbus/canbuslib.h"
#include "drivers/logger/logger.h"
#include "evergreen/list_generic.h"


#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

#define CANBUS_NOTIFY_BIT_SUCCESS   0
#define CANBUS_NOTIFY_BIT_FAILURE   1

enum canbus_request_code
{
    CANBUS_CONFIGURE_CHIP,
    CANBUS_GET_CONFIGURATION,
    CANBUS_WRITE_BUFFER,
    CANBUS_DATA_RECEIVED,
    CANBUS_DATA_SENT
};

struct canbus_request
{
    uint8_t                     num;
    enum canbus_request_code    canbus_request_code;
    void*                       data;
    TaskHandle_t                task_id;
};

struct canbus_chip
{
    struct canbus_chip_info*    canbus_chip_info;
    struct canbus_config        canbus_config;

    bool                        configured;

    struct list_head            list;
};

struct canbus_listener
{
    uint8_t     num;
    uint8_t     event;
    uint8_t*    data;
    void        (*func)(uint8_t num, uint8_t event, uint8_t* data, uint8_t items);

    struct list_head list;
};

struct canbus_tx_data
{
    uint8_t*    data;
    uint8_t     items;
};

LIST_HEAD(canbus_chips);
LIST_HEAD(canbus_listeners);
static QueueHandle_t canbus_service_queue;

void canbuschip_add(struct canbus_chip_info* canbus_chip_info)
{
    struct canbus_chip* canbus_chip = (struct canbus_chip*)pvPortMalloc(sizeof(struct canbus_chip));
    if(canbus_chip == NULL)
    {
        while(1);
    }

    canbus_chip->canbus_chip_info = canbus_chip_info;
    canbus_chip->configured       = false;

    list_add(&(canbus_chip->list), &canbus_chips);
}

static uint8_t __canbus_send_service_request(uint8_t num, uint8_t request_code, void* data)
{
    struct canbus_request request =
    {
        .num                    = num,
        .canbus_request_code    = request_code,
        .task_id                = xTaskGetCurrentTaskHandle(),
        .data                   = data
    };

    while(xQueueSend(canbus_service_queue, (const void*)&request, portMAX_DELAY) != pdTRUE)
        vTaskDelay(1);

    uint32_t notified_value;
    xTaskNotifyWait(CANBUS_NOTIFY_BIT_FAILURE | CANBUS_NOTIFY_BIT_SUCCESS,
                    ULONG_MAX,
                    &notified_value,
                    portMAX_DELAY);

    return -(notified_value & CANBUS_NOTIFY_BIT_FAILURE);
}

uint8_t canbus_init(uint8_t id, struct canbus_config* config)
{
    return __canbus_send_service_request(id, CANBUS_CONFIGURE_CHIP, (void*)config);
}

uint8_t canbus_write_data(uint8_t id, struct canbus_message_object* msg)
{
    return __canbus_send_service_request(id, CANBUS_WRITE_BUFFER, (void*)msg);
}

static struct canbus_chip* __canbus_find_chip(uint8_t num)
{
    struct canbus_chip* canbus_chip;
    list_for_each_entry(canbus_chip, &canbus_chips, list)
    {
        if(canbus_chip->canbus_chip_info->id == num)
            return canbus_chip;
    }

    return NULL;
}

static uint8_t __canbus_init(uint8_t num, struct canbus_config* config)
{
    struct canbus_chip* canbus_chip = __canbus_find_chip(num);
    if(canbus_chip == NULL)
        return CANBUS_NOTIFY_BIT_FAILURE;

    /*
     *TODO: should check RX and TX pins like in UART service
     * */

    canbus_chip->canbus_chip_info->canbus_init(num, config);
    canbus_chip->canbus_config = *config;
    canbus_chip->configured    = true;

    return CANBUS_NOTIFY_BIT_SUCCESS;
}

static uint8_t __canbus_write_buffer(uint8_t num, struct canbus_message_object* msg)
{
    struct canbus_chip* canbus_chip = __canbus_find_chip(num);

    if(canbus_chip == NULL || canbus_chip->configured == false || msg == NULL || msg->buffer == NULL)
        return CANBUS_NOTIFY_BIT_FAILURE;

    return canbus_chip->canbus_chip_info->canbus_write(num, msg);
}

void canbus_service(void* params)
{
    struct canbus_request request;
    uint8_t result;
    uint8_t i;

    while(true)
    {
        while(xQueueReceive(canbus_service_queue, (void*)&request, portMAX_DELAY) != pdTRUE)
            vTaskDelay(1);

        switch(request.canbus_request_code)
        {
        case CANBUS_CONFIGURE_CHIP:
            result = __canbus_init(request.num, (struct canbus_config*)(request.data));
            break;
        case CANBUS_WRITE_BUFFER:
            result = __canbus_write_buffer(request.num, (struct canbus_message_object*)(request.data));
            break;
        }

        xTaskNotify(request.task_id, (uint32_t)result, eSetValueWithOverwrite);
    }
}

void canbus_service_init(void)
{
    canbus_module_init();
    canbus_service_queue = xQueueCreate(10, sizeof(struct canbus_request));
    xTaskCreate(canbus_service, "", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY, NULL);
}
