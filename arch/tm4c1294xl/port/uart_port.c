#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "drivers/uart/uart_generic.h"
#include "drivers/uart/uartlib.h"

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_uart.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/udma.h"

#define TOTAL_UART_CHIPS        8
#define UART_RX_BUFFER_SIZE     64
#define UART_RX_TRANSFER_SIZE   8

#define UART_RX_EVENT           1
#define UART_TX_EVENT           2

struct rx_data
{
    uint8_t buffer[UART_RX_BUFFER_SIZE];
    uint8_t last_position;
    uint8_t last_read_byte;
};

static const base[] =
{
    UART0_BASE,
    UART1_BASE,
    UART2_BASE,
    UART3_BASE,
    UART4_BASE,
    UART5_BASE,
    UART6_BASE,
    UART7_BASE
};

static const gpio_base[] =
{
    GPIO_PORTA_BASE,
    GPIO_PORTB_BASE,
    GPIO_PORTC_BASE,
    GPIO_PORTD_BASE,
    GPIO_PORTE_BASE,
    GPIO_PORTF_BASE,
    GPIO_PORTG_BASE,
    GPIO_PORTH_BASE,
    GPIO_PORTJ_BASE,
    GPIO_PORTK_BASE,
    GPIO_PORTL_BASE,
    GPIO_PORTM_BASE,
    GPIO_PORTN_BASE,
    GPIO_PORTP_BASE,
    GPIO_PORTQ_BASE
};

static const sysctl[] =
{
    SYSCTL_PERIPH_UART0,
    SYSCTL_PERIPH_UART1,
    SYSCTL_PERIPH_UART2,
    SYSCTL_PERIPH_UART3,
    SYSCTL_PERIPH_UART4,
    SYSCTL_PERIPH_UART5,
    SYSCTL_PERIPH_UART6,
    SYSCTL_PERIPH_UART7,

    SYSCTL_PERIPH_GPIOA,
    SYSCTL_PERIPH_GPIOB,
    SYSCTL_PERIPH_GPIOC,
    SYSCTL_PERIPH_GPIOD,
    SYSCTL_PERIPH_GPIOE,
    SYSCTL_PERIPH_GPIOF,
    SYSCTL_PERIPH_GPIOG,
    SYSCTL_PERIPH_GPIOH,
    SYSCTL_PERIPH_GPIOJ,
    SYSCTL_PERIPH_GPIOK,
    SYSCTL_PERIPH_GPIOL,
    SYSCTL_PERIPH_GPIOM,
    SYSCTL_PERIPH_GPION,
    SYSCTL_PERIPH_GPIOP,
    SYSCTL_PERIPH_GPIOQ
};

static const uint32_t udma_channel[][4] =
{
    { 8,  9,  0x08,    0x09    },
    { 22, 23, 0x016,   0x017   },
    { 12, 13, 0x1000C, 0x1000D },
    { 16, 17, 0x20010, 0x20011 },
    { 18, 19, 0x20012, 0x20013 },
    { 6,  7,  0x20006, 0x20007 },
    { 10, 11, 0x2000A, 0x2000B },
    { 20, 21, 0x20014, 0x20015 }
};

static const int_base[] =
{
    INT_UART0,
    INT_UART1,
    INT_UART2,
    INT_UART3,
    INT_UART4,
    INT_UART5,
    INT_UART6,
    INT_UART7
};

static struct rx_data* rx_data[] =
{
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

static bool transmitting[] =
{
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
};

static uint8_t* tx_buffers[] =
{
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL
};

extern void udma_init();

static inline void __enable_clock(uint8_t i)
{
    if(!SysCtlPeripheralReady(sysctl[i]))
    {
        SysCtlPeripheralDisable(sysctl[i]);
        SysCtlPeripheralReset(sysctl[i]);
        SysCtlPeripheralEnable(sysctl[i]);
        while(!SysCtlPeripheralReady(sysctl[i]));
    }
}

static void __enable_pin_clock(uint8_t pin)
{
    uint8_t index = TOTAL_UART_CHIPS +  pin / 8;
    __enable_clock(index);
}

static void uart_chip_enable_clock(uint8_t num)
{
    __enable_clock(num);
}

static void uart_gpio_enable_clock(uint8_t rx_pin, uint8_t tx_pin)
{
    __enable_pin_clock(rx_pin);
    __enable_pin_clock(tx_pin);
}

static uint8_t uart_config(uint8_t num, struct uart_config* config)
{
    uart_chip_enable_clock(num);
    uart_gpio_enable_clock(config->rx_pin, config->tx_pin);
    udma_init();

    /* RX pin */
    uint64_t port = (config->rx_pin) / 8; /* port: from 0 to x */
    uint64_t pin = (config->rx_pin) % 8;  /* pin: from 0 to 7  */
    uint64_t value = 0x01 | (port << 16) | (0x04 * pin) << 8;
    GPIOPinConfigure(value);
    GPIOPinTypeUART(gpio_base[port], 1 << pin);

    /* TX pin */
    port = (config->tx_pin) / 8; /* port: from 0 to x */
    pin = (config->tx_pin) % 8;  /* pin: from 0 to 7  */
    value = 0x01 | (port << 16) | ((0x04 * pin) << 8) | (0x04 << 8);
    GPIOPinConfigure(value);
    GPIOPinTypeUART(gpio_base[port], 1 << pin);

    uint8_t stop_bits = 0x08 * (config->stop_bits - 1);
    uint8_t parity    = config->parity_mode;
    UARTConfigSetExpClk(base[num], 120000000, config->baud_rate,
                        UART_CONFIG_WLEN_8 |  stop_bits | parity);

    UARTFIFOLevelSet(base[num], UART_FIFO_TX7_8, UART_FIFO_RX4_8);
    UARTTxIntModeSet(base[num], UART_TXINT_MODE_EOT);
    UARTEnable(base[num]);
    if(UARTTxIntModeGet(base[num]) != UART_TXINT_MODE_EOT)
        system_reset();

    if(rx_data[num] == NULL)
    {
        rx_data[num] = (uint8_t*)pvPortMalloc(sizeof(struct rx_data));
        if(rx_data[num] == NULL)
        {
            system_reset();
        }
        rx_data[num]->last_position = 0;
    }

    /* Use DMA for UART, so configure it. */
    uDMAChannelAssign(udma_channel[num][2]);  /* map rx channel */
    UARTDMAEnable(base[num], UART_DMA_RX | UART_DMA_TX);

    uDMAChannelAttributeDisable(udma_channel[num][0], UDMA_ATTR_HIGH_PRIORITY | UDMA_ATTR_ALTSELECT | UDMA_ATTR_REQMASK);
    uDMAChannelAttributeEnable(udma_channel[num][0], UDMA_ATTR_USEBURST);
    uDMAChannelControlSet(udma_channel[num][0] | UDMA_PRI_SELECT,
                          UDMA_SIZE_8 |
                          UDMA_SRC_INC_NONE |
                          UDMA_DST_INC_8 |
                          UDMA_ARB_8);
    uDMAChannelTransferSet(udma_channel[num][0] | UDMA_PRI_SELECT,
                           UDMA_MODE_BASIC,
                           (void*)(base[num] + UART_O_DR),
                           rx_data[num]->buffer,
                           UART_RX_TRANSFER_SIZE);
    uDMAChannelEnable(udma_channel[num][0]);

    UARTIntClear(base[num], UART_INT_DMARX | UART_INT_DMATX);
    UARTIntEnable(base[num], UART_INT_DMARX | UART_INT_DMATX);
    IntEnable(int_base[num]);

    return 0;
}

void uart_isr_handler(void)
{
    IntMasterDisable();
    volatile uint8_t i;
    volatile uint32_t status;
    volatile uint32_t mode;

    for(i = 0; i < TOTAL_UART_CHIPS; i++)
    {
        if(!SysCtlPeripheralReady(sysctl[i]))
            continue;

        status = UARTIntStatus(base[i], true);
        UARTIntClear(base[i], status);

        mode = uDMAChannelModeGet(udma_channel[i][0] | UDMA_PRI_SELECT);
        mode = uDMAChannelModeGet(udma_channel[i][1] | UDMA_PRI_SELECT);
        if(!uDMAChannelIsEnabled(udma_channel[i][0]))
        {
            uart_notify_data_received(i, rx_data[i]->buffer + rx_data[i]->last_position);
            rx_data[i]->last_position += UART_RX_TRANSFER_SIZE;
            if(rx_data[i]->last_position >= UART_RX_BUFFER_SIZE)
            {
                rx_data[i]->last_position = 0;
            }
            uDMAChannelTransferSet(udma_channel[i][0] | UDMA_PRI_SELECT,
                                   UDMA_MODE_BASIC,
                                   (void*)(base[i] + UART_O_DR),
                                   rx_data[i]->buffer + rx_data[i]->last_position,
                                   UART_RX_TRANSFER_SIZE);
            uDMAChannelEnable(udma_channel[i][0]);
        }

        if((transmitting[i] == true) && (!uDMAChannelIsEnabled(udma_channel[i][1])))
        {
            UARTIntClear(base[i], UART_DMA_TX);
            uart_notify_data_sent(i, tx_buffers[i]);
            transmitting[i] = false;
        }
    }
    IntMasterEnable();
}

static uint8_t uart_rx_bytes(uint8_t num)
{
    int8_t temp = rx_data[num]->last_position - rx_data[num]->last_read_byte;
    temp = temp < 0 ? UART_RX_BUFFER_SIZE + temp : temp;

    return temp;
}

static uint8_t uart_write(uint8_t num, uint8_t* data, uint8_t items)
{
    /* DMA */
    /* If peripheral is already active, aka if UART is already transmitting? */
    IntMasterDisable();
    uint8_t result = 0;
    if(uDMAChannelIsEnabled(udma_channel[num][1]))
    {
        result = 1;
        goto exit;
    }

    uDMAChannelAssign(udma_channel[num][3]);  /* map rx channel */
    uDMAChannelAttributeDisable(udma_channel[num][1], UDMA_ATTR_HIGH_PRIORITY | UDMA_ATTR_ALTSELECT | UDMA_ATTR_REQMASK);
    uDMAChannelAttributeEnable(udma_channel[num][1], UDMA_ATTR_USEBURST | UDMA_ATTR_HIGH_PRIORITY);
    uDMAChannelControlSet(udma_channel[num][1] | UDMA_PRI_SELECT,
                          UDMA_SIZE_8 |
                          UDMA_SRC_INC_8 |
                          UDMA_DST_INC_NONE |
                          UDMA_ARB_1);
    uDMAChannelTransferSet(udma_channel[num][1] | UDMA_PRI_SELECT,
                          UDMA_MODE_BASIC,
                          data,
                          (void*)(base[num] + UART_O_DR),
                          items);
    uDMAChannelEnable(udma_channel[num][1]);
    transmitting[num] = true;
    tx_buffers[num] = data;

exit:
    IntMasterEnable();
    return result;
}

static uint8_t uart_buffer_read(uint8_t num, uint8_t* buffer)
{
    if(rx_data[num]->last_position == rx_data[num]->last_read_byte)
        return 1;

    IntDisable(int_base[num]);
    uint8_t* rx_buffer_start = rx_data[num]->buffer + rx_data[num]->last_position - UART_RX_TRANSFER_SIZE;
    IntEnable(int_base[num]);

    memcpy((void*)buffer, (void*)rx_buffer_start, UART_RX_TRANSFER_SIZE);
    rx_data[num]->last_read_byte += UART_RX_TRANSFER_SIZE;
    if(rx_data[num]->last_read_byte >= UART_RX_BUFFER_SIZE)
        rx_data[num]->last_read_byte = 0;

    return 0;
}

static const uint8_t uart0_rx_pins[] = { 0, 116 };
static const uint8_t uart0_tx_pins[] = { 1 };
static struct uart_chip_info uart0 =
{
    .uart_init          = uart_config,
    .uart_write_buffer  = uart_write,

    .rx_pins            = uart0_rx_pins,
    .tx_pins            = uart0_tx_pins,
    .total_rx_pins      = 2,
    .total_tx_pins      = 1,
    .transfer_size      = UART_RX_TRANSFER_SIZE,

    .id                 = 0,
};

static const uint8_t uart1_rx_pins[] = { 8 };
static const uint8_t uart1_tx_pins[] = { 9 };
static struct uart_chip_info uart1 =
{
    .uart_init          = uart_config,
    .uart_write_buffer  = uart_write,

    .rx_pins            = uart1_rx_pins,
    .tx_pins            = uart1_tx_pins,
    .total_rx_pins      = 1,
    .total_tx_pins      = 1,
    .transfer_size      = UART_RX_TRANSFER_SIZE,

    .id                 = 1,
};

static const uint8_t uart2_rx_pins[] = { 6, 28 };
static const uint8_t uart2_tx_pins[] = { 7, 29 };
static struct uart_chip_info uart2 =
{
    .uart_init          = uart_config,
    .uart_write_buffer  = uart_write,

    .rx_pins            = uart2_rx_pins,
    .tx_pins            = uart2_tx_pins,
    .total_rx_pins      = 2,
    .total_tx_pins      = 2,
    .transfer_size      = UART_RX_TRANSFER_SIZE,

    .id                 = 2,
};

static const uint8_t uart5_rx_pins[] = { 22 };
static const uint8_t uart5_tx_pins[] = { 23 };
static struct uart_chip_info uart5 =
{
    .uart_init          = uart_config,
    .uart_write_buffer  = uart_write,

    .rx_pins            = uart5_rx_pins,
    .tx_pins            = uart5_tx_pins,
    .total_rx_pins      = 1,
    .total_tx_pins      = 1,
    .transfer_size      = UART_RX_TRANSFER_SIZE,

    .id                 = 5,
};

void uart_module_init(void)
{
    uartchip_add(&uart0);
    uartchip_add(&uart1);
    uartchip_add(&uart2);
    uartchip_add(&uart5);
}
