#include "kernel/arch_sys_control.h"
#include "config/evergreen.h"

#include <stdio.h>
#include <stdbool.h>

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"

void early_init(void)
{
    SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
                        SYSCTL_OSC_MAIN |
                        SYSCTL_USE_PLL |
                        SYSCTL_CFG_VCO_480), 120000000);
    IntMasterEnable();
}

void system_reset(void)
{
    //SycCtlReset();
}

void system_suspend(void)
{
#if (EVERNGREEN_SLEEP_ENABLED == 1)
    SysCtlSleep();
#endif
}

void system_save_buffer_to_nv_memory(uint8_t address, uint8_t* buffer, uint8_t items)
{
#if (EVERGREEN_NV_MEM_ENABLED == 1)
#endif
}
