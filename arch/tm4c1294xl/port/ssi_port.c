#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/ssi.h"
#include "driverlib/sysctl.h"
#include "configure/sdc_config.h"


void inline SELECT(void)
{
    GPIOPinWrite(SDC_SSI_FSS_GPIO_PORT_BASE, SDC_SSI_FSS, 0);
}

void inline DESELECT(void)
{
    GPIOPinWrite(SDC_SSI_FSS_GPIO_PORT_BASE, SDC_SSI_FSS, SDC_SSI_FSS);
}

void send_initial_clock_train(void)
{
    unsigned int i;
    uint32_t ui32Dat;

    DESELECT();

    GPIOPinTypeGPIOOutput(SDC_SSI_TX_GPIO_PORT_BASE, SDC_SSI_TX);
    GPIOPinWrite(SDC_SSI_TX_GPIO_PORT_BASE, SDC_SSI_TX, SDC_SSI_TX);

    for(i = 0 ; i < 10 ; i++)
    {
        SSIDataPut(SDC_SSI_BASE, 0xFF);
        SSIDataGet(SDC_SSI_BASE, &ui32Dat);
    }

    GPIOPinTypeSSI(SDC_SSI_TX_GPIO_PORT_BASE, SDC_SSI_TX);
}

void power_on(void)
{
    SysCtlPeripheralEnable(SDC_SSI_SYSCTL_PERIPH);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

    GPIOPinConfigure(GPIO_PB5_SSI1CLK);
    GPIOPinConfigure(GPIO_PE4_SSI1XDAT0);
    GPIOPinConfigure(GPIO_PE5_SSI1XDAT1);

    GPIOPinTypeSSI(SDC_SSI_TX_GPIO_PORT_BASE, SDC_SSI_TX);
    GPIOPinTypeSSI(SDC_SSI_RX_GPIO_PORT_BASE, SDC_SSI_RX);
    GPIOPinTypeSSI(SDC_SSI_CLK_GPIO_PORT_BASE, SDC_SSI_CLK);

    GPIOPinTypeGPIOOutput(SDC_SSI_FSS_GPIO_PORT_BASE, SDC_SSI_FSS);

    GPIOPadConfigSet(SDC_SSI_RX_GPIO_PORT_BASE, SDC_SSI_RX,
                     GPIO_STRENGTH_4MA, GPIO_PIN_TYPE_STD_WPU);
    GPIOPadConfigSet(SDC_SSI_CLK_GPIO_PORT_BASE, SDC_SSI_CLK,
                     GPIO_STRENGTH_4MA, GPIO_PIN_TYPE_STD);
    GPIOPadConfigSet(SDC_SSI_TX_GPIO_PORT_BASE, SDC_SSI_TX,
                     GPIO_STRENGTH_4MA, GPIO_PIN_TYPE_STD);
    GPIOPadConfigSet(SDC_SSI_FSS_GPIO_PORT_BASE, SDC_SSI_FSS,
                     GPIO_STRENGTH_4MA, GPIO_PIN_TYPE_STD);

    SSIConfigSetExpClk(SDC_SSI_BASE, g_ui32SysClock,
                           SSI_FRF_MOTO_MODE_0, SSI_MODE_MASTER, 400000, 8);
    SSIEnable(SDC_SSI_BASE);

    send_initial_clock_train();
}

void set_max_speed(void)
{
    unsigned long i;

    SSIDisable(SDC_SSI_BASE);

    i = g_ui32SysClock / 2;
    if(i > 12500000)
    {
        i = 12500000;
    }

    SSIConfigSetExpClk(SDC_SSI_BASE, g_ui32SysClock,
                       SSI_FRF_MOTO_MODE_0, SSI_MODE_MASTER, i, 8);
    SSIEnable(SDC_SSI_BASE);
}
