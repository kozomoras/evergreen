#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#include "hw_types.h"
#include "hw_ints.h"
#include "hw_memmap.h"
#include "hw_common_reg.h"
#include "gpio.h"
#include "pin.h"
#include "prcm.h"

#include "drivers/gpio/gpio_generic.h"

static const base[] =
{
    GPIOA0_BASE,
    GPIOA1_BASE,
    GPIOA2_BASE,
    GPIOA3_BASE,
    GPIOA4_BASE
};

static uint32_t prcm[] =
{
    PRCM_GPIOA0,
    PRCM_GPIOA1,
    PRCM_GPIOA2,
    PRCM_GPIOA3,
    PRCM_GPIOA4
};

static unsigned char pads[] =
{
    50,  // GPIO0
    55,  // GPIO1
    57,  // GPIO2
    58,  // GPIO3
    59,  // GPIO4
    60,  // GPIO5
    61,  // GPIO6
    62,  // GPIO7
    63,  // GPIO8
    64,  // GPIO9
    1,   // GPIO10
    2,   // GPIO11
    3,   // GPIO12
    4,   // GPIO13
    5,   // GPIO14
    6,   // GPIO15
    7,   // GPIO16
    8,   // GPIO17
    11,  // GPIO18
    12,  // GPIO19
    13,  // GPIO20
    14,  // GPIO21
    15,  // GPIO22
    16,  // GPIO23
    17,  // GPIO24
    21,  // GPIO25
    29,  // GPIO26
    30,  // GPIO27
    18,  // GPIO28
    20,  // GPIO29
    53,  // GPIO30
    45,  // GPIO31
    52   // GPIO32
};

static void __gpio_get_port_pin(uint8_t index, uint32_t* base_reg, uint8_t* pin, uint32_t* prcm_reg)
{
    uint8_t temp = index / 8;

    *prcm_reg = prcm[temp];
    *base_reg = base[temp];
    *pin      = 1 << (index % 8);
}

static void __gpio_enable_clock(uint8_t index)
{
    uint32_t prcm_addr = prcm[index / 8];
    if(PRCMPeripheralStatusGet(prcm_addr) == false)
    {
        PRCMPeripheralClkEnable(prcm_addr, PRCM_RUN_MODE_CLK);
        PRCMPeripheralReset(prcm_addr);
    }
}

static void __gpio_register_input(uint8_t index)
{
    __gpio_enable_clock(index);
    PinTypeGPIO(pads[index] - 1, 0, false);
    GPIODirModeSet(base[index / 8], 1 << (index % 8), GPIO_DIR_MODE_IN);
}

static void __gpio_register_output(uint8_t index)
{
    __gpio_enable_clock(index);
    PinTypeGPIO(pads[index] - 1, 0, false);
    GPIODirModeSet(base[index / 8], 1 << (index % 8), GPIO_DIR_MODE_OUT);
}

static void __gpio_write(uint8_t pin, bool value)
{
    uint8_t temp = pin / 8;
    GPIOPinWrite(base[temp], 1 << (pin % 8), (value == false ? 0 : 0xFF));
}

static uint8_t __gpio_read(uint8_t pin)
{
    uint8_t temp = GPIOPinRead(base[pin / 8], 1 << (pin % 8)) == 0 ? 0 : 1;

    return temp;
}

static struct gpio_chip_info gpio_chip_info =
{
    .gpio_register_input    = __gpio_register_input,
    .gpio_register_output   = __gpio_register_output,
    .gpio_unregister        = NULL,
    .gpio_write             = __gpio_write,
    .gpio_request_isr       = NULL,
    .gpio_release_isr       = NULL,
    .gpio_read              = __gpio_read,
    .label                  = "cc3200/gpio",
    .start_pin              = 0,
    .total_pins             = 32
};

void gpio_module_init(void)
{
    gpiochip_add(&gpio_chip_info);
}
