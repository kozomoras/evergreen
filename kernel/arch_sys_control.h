#ifndef _ARCH_SYS_CONTROL_DEFINED
#define _ARCH_SYS_CONTROL_DEFINED

#include <stdint.h>

void early_init(void);
void system_reset(void);
void system_suspend(void);
void system_save_buffer_to_nv_memory(uint8_t address, uint8_t* buffer, uint8_t item);
void system_watchdog_clear(void);

#endif
